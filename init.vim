call plug#begin('~/.config/nvim/plugged')

" Project wide search
Plug 'mileszs/ack.vim'

" A file and folder explorer
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" Show Git statuses in the nerdtree file explorer
Plug 'Xuyuanp/nerdtree-git-plugin'

" Show changes in Git
Plug 'airblade/vim-gitgutter'

" A command line fuzzy string matcher
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" A status-line plugin
Plug 'itchyny/lightline.vim'

" A colour theme
Plug 'MaxSt/FlatColor'

" Highlight trailing whitespace
Plug 'ntpeters/vim-better-whitespace'

" Elm language plugin
Plug 'ElmCast/elm-vim', { 'for': 'elm' }

" Rust language plugin
Plug 'rust-lang/rust.vim', { 'for': 'rust' }

" Elixir language plugin
Plug 'elixir-lang/vim-elixir', { 'for': 'elixir' }

" Toml markup plugin
Plug 'cespare/vim-toml'

" Markdown markup plugin
Plug 'plasticboy/vim-markdown'

" Add Git commands
Plug 'tpope/vim-fugitive'

" Add the end keyword automatically in Ruby
Plug 'tpope/vim-endwise', { 'for': 'ruby' }

" For code autocompletion
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

" For rust autocompletion with deoplete
Plug 'sebastianmarkow/deoplete-rust', { 'for': 'rust' }

" Vue syntax highlighting
Plug 'posva/vim-vue'

" Ruby language plugin
Plug 'vim-ruby/vim-ruby', { 'for': 'ruby' }

" Ruby on Rails framework plugin
Plug 'tpope/vim-rails', { 'for': 'ruby' }

" Javascript language plugin
Plug 'jelera/vim-javascript-syntax', { 'for': 'javascript' }

" Comment out blocks of code
Plug 'scrooloose/nerdcommenter'

" A multi-language syntax checker
Plug 'neomake/neomake'

" A RSpec test runner
Plug 'thoughtbot/vim-rspec'

" Neovim embedded terminal helper
Plug 'kassio/neoterm'

call plug#end()


" >----- neoterm -----

let g:neoterm_position = 'horizontal'
let g:neoterm_automap_keys = ',tt'

" hide/close terminal
nnoremap <silent> ,th :call neoterm#close()<cr>

" clear terminal
nnoremap <silent> ,tl :call neoterm#clear()<cr>

" >----- neoterm -----


" >----- vim-rspec -----

map <Leader>t :call RunCurrentSpecFile()<CR>
map <Leader>s :call RunNearestSpec()<CR>
map <Leader>l :call RunLastSpec()<CR>
map <Leader>a :call RunAllSpecs()<CR>

let g:rspec_command = ":T bundle exec rspec --order=defined {spec}"

" <----- vim-rspec -----


" >----- neomake -----

autocmd! BufWritePost * Neomake

" <----- neomake -----


" >----- nerdcommenter -----

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

map <D-/> <plug>NERDCommenterToggle<CR>
imap <D-/> <Esc><plug>NERDCommenterToggle<CR>i

" >----- nerdcommenter -----


" >----- rust.vim -----

" Tell it where the rustc binary is
let g:rustc_path = $HOME . "/.cargo/bin/racer"

" Format Rust code automatically on save
let g:rustfmt_autosave = 1

" <----- rust.vim -----


" >----- deoplete-rust -----

" Setting for my local install of Rust
let g:deoplete#sources#rust#racer_binary = $HOME . "/.cargo/bin/racer"
let g:deoplete#sources#rust#rust_source_path = $HOME . "/.multirust/toolchains/stable-x86_64-apple-darwin/lib/rustlib/src/rust/src"

" <----- deoplete-rust -----


" >----- ack.vim -----

if executable('rg')
  let g:ackprg = 'rg --vimgrep'
endif

" <----- ack.vim -----


" >----- fzf -----

" Make it show up the same way as Ctl-P
nnoremap <c-p> :FZF<cr>

" <----- fzf -----


" >----- elm-vim -----

" Format Elm code automatically on save
let g:elm_format_autosave = 1

" <----- elm-vim -----


" >----- FlatColor -----

" Use the same colour scheme with the lightline.vim plugin
let g:lightline = { 'colorscheme': 'flatcolor' }

" Use the flat color theme
colorscheme flatcolor

if has('nvim')
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif

" <----- FlatColor -----


" >----- nerdtree -----

" Open up the folder drawer
map <C-n> :NERDTreeToggle<CR>

" Also allow using a leader n to open the drawer
nnoremap <leader>n :NERDTreeToggle<CR>

" Quit vim if only the drawer is left open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" <----- nerdtree -----


" >----- deoplete.nvim -----

" Turn on deoplete at startup
let g:deoplete#enable_at_startup = 1

" Auto select the first result, then Enter to autocomplete
set completeopt+=noinsert

" <----- deoplete.nvim -----


" >----- vim-markdown -----

let g:vim_markdown_folding_disabled = 1
set conceallevel=2

" <----- vim-markdown -----


" >----- GENERAL -----

" Change the key that starts Vim commands
let mapleader = "\<Space>"

" Save a file easly
nnoremap <Leader>w :w<CR>

" Split Vim vertically or horizontally with the greatest of ease
noremap <leader>v :vsp<cr>
noremap <leader>h :sp<cr>

set expandtab           " Insert spaces when TAB is pressed.
set shiftwidth=2        " Indentation amount for < and > commands.
set tabstop=2           " Render TABs using this many spaces.
set autoindent          " Auto indent
set number              " Show the line numbers on the left side.
set showmatch           " Show matching brackets.
set cb=unnamedplus      " Share vim copy registers with OS X
set mouse=a             " Enable mouse support
set nowrap              " Don't wrap long lines

set t_Co=256
syntax on
set background=dark
filetype plugin indent on

" Tell Vim which characters to show for expanded TABs,
" trailing whitespace, and end-of-lines. VERY useful!
if &listchars ==# 'eol:$'
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif

set list " Show problematic characters.

" Also highlight all tabs and trailing whitespace characters.
highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
match ExtraWhitespace /\s\+$\|\t/

" Show a line indicator at 120 characters width
set colorcolumn=120
highlight ColorColumn ctermbg=0 guibg=grey

" Use a custom cursor color in terminal mode
highlight TermCursor ctermfg=green guifg=green

" Make those debugger statements painfully obvious
au BufEnter *.rb syn match error contained "\<puts\>"
au BufEnter *.rb syn match error contained "\<save_and_open_page\>"
au BufEnter *.rb syn match error contained "\<binding.pry\>"
au BufEnter *.rb syn match error contained "\<debugger\>"
au BufEnter *.js syn match error contained "\<console\>"
au BufEnter *.js syn match error contained "\<debugger\>"

" <----- GENERAL -----
