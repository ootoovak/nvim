(*
  Indicates if the active window of the active application is currently in fullscreen mode.
  Fails silently in case of error and returns false.
*)
on isFullScreen()
  tell application "System Events"
    try
      tell front window of (first process whose frontmost is true)
        return get value of attribute "AXFullScreen"
      end tell
    end try
  end tell
  return false
end isFullScreen

tell application "Neovim"
  activate
  delay 1
  if not my isFullScreen() then
    tell application "System Events" to keystroke "f" using {command down, control down}
  end if
end tell
